## Building

To build and run project, you need to install [docker](https://docs.docker.com/install/) and [docker-compose](https://docs.docker.com/compose/install/).
Then, open project directory in terminal and write command:

```bash
docker-compose up

```

___

by @encrypted.fox
@byshabay 
2019