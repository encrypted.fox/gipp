import React, {Component, Fragment} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Admin from "./Admin/Admin.js";
import Menu from "./Menu/Menu.js";
import About from "./About/About.js";
import Blog from "./Blog/Blog.js";
import Contacts from "./Contacts/Contacts.js";
import Footer from "./Footer/Footer.js";
import './App.css';

document.addEventListener("DOMContentLoaded", documentContentReady);

function documentContentReady() {
    let beforeLoadPlaceholder = document.getElementById("beforeLoadPlaceholder");
    beforeLoadPlaceholder.className = "contentNone";
    setTimeout(() => beforeLoadPlaceholder.className = "contentDisplayNone", 400);
}

class App extends Component {
    render() {
        return (
            <Router>
                <Fragment>
                    <Menu/>
                    <main>
                        <Switch>
                            <Route exact path="/" component={Blog}/>
                            <Route path="/about" component={About}/>
                            <Route path="/contacts" component={Contacts}/>
                            <Route path="/admin" component={Admin}/>
                        </Switch>
                    </main>
                    <Footer/>
                </Fragment>
            </Router>
        );
    }
}

export default App;
