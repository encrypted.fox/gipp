import React, {Component} from "react";
import Markdown from "react-markdown";
import "./Admin.css";
import ShareButton from "../Blog/PostsList/Post/ShareButton/ShareButton.js";
import {NavLink} from "react-router-dom";

class Admin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            adminPageNameOfPublication: "Название публикации",
            adminPageDateOfPublication: "12.11.1999",
            adminPageTimeOfPublication: "12:20",
            adminPageText: "# Reasons React is great\n\n" +
                "## Reasons React is great\n\n" +
                "### Reasons React is great\n\n" +
                "#### Reasons React is great\n\n" +
                "##### Reasons React is great\n\n" +
                "Reasons React is great\n\n" +
                "[google](https://google.com)\n\n" +
                "1. Server-side rendering\n\n" +
                "2. This totally works:\n\n" +
                "- This\n\n",
            adminPageTags: ["#tag"]
        };

        this.handleNameOfPublicationChange = this.handleNameOfPublicationChange.bind(this);
        this.handleDateOfPublicationChange = this.handleDateOfPublicationChange.bind(this);
        this.handleTimeOfPublicationChange = this.handleTimeOfPublicationChange.bind(this);
        this.handleTextChange = this.handleTextChange.bind(this);
        this.handleTagsChange = this.handleTagsChange.bind(this);
        this.handlePublishClick = this.handlePublishClick.bind(this);
    }

    handleNameOfPublicationChange(event) {
        this.setState({
            adminPageNameOfPublication: event.target.value
        });
    }

    handleDateOfPublicationChange(event) {
        this.setState({
            adminPageDateOfPublication: event.target.value.split("-").reverse().join(".")
        });
    }

    handleTimeOfPublicationChange(event) {
        this.setState({
            adminPageTimeOfPublication: event.target.value
        });
    }

    handleTextChange(event) {
        this.setState({
            adminPageText: event.target.value
        });
    }

    handleTagsChange(event) {
        this.setState({
            adminPageTags: event.target.value.trim().split(" ")
        });
    }

    handlePublishClick(event) {
        event.preventDefault();
    }

    render() {
        return <section id="adminSection">
            <div className="container">
                <form action="">
                    <h2><label htmlFor="nameOfPublication">Название публикации</label></h2>
                    <input type="text"
                           name="nameOfPublication"
                           id="nameOfPublication"
                           placeholder={this.state.adminPageNameOfPublication}
                           onChange={this.handleNameOfPublicationChange}/>
                    <h2><label htmlFor="dateOfPublication">Дата публикации</label></h2>
                    <input type="date"
                           name="dateOfPublication"
                           id="dateOfPublication"
                           placeholder={this.state.adminPageDateOfPublication}
                           onChange={this.handleDateOfPublicationChange}/>
                    <label htmlFor="timeOfPublication"/>
                    <input type="time"
                           name="timeOfPublication"
                           id="timeOfPublication"
                           placeholder={this.state.adminPageTimeOfPublication}
                           onChange={this.handleTimeOfPublicationChange}/>
                    <h2><label htmlFor="adminPageText">Введите текст</label></h2>
                    <textarea name="adminPageText"
                              id="adminPageText"
                              value={this.state.adminPageText}
                              onChange={this.handleTextChange}/>
                    <h2><label htmlFor="postTags">Тэги</label></h2>
                    <input type="text"
                           name="postTags"
                           id="postTags"
                           placeholder={this.state.adminPageTags.join(" ")}
                           onChange={this.handleTagsChange}/>
                    <br/>
                    <div>
                        <NavLink to="/"
                                 className="adminButton adminButtonWarn">Отменить
                        </NavLink>
                        <button className="adminButton adminButtonSucc"
                                onClick={this.handlePublishClick}>Опубликовать
                        </button>
                    </div>
                </form>


                {/**/}


                <h2>Превью</h2>
                <div className="preview">
                    <h2>{this.state.adminPageNameOfPublication}</h2>
                    <hr/>
                    <p className="date">{this.state.adminPageTimeOfPublication + " " + this.state.adminPageDateOfPublication}</p>
                    <div className="text">
                        <Markdown source={this.state.adminPageText} escapeHtml={false}/>
                    </div>
                    <div className="flexPostFooter">
                        <div className="tags">{
                            this.state.adminPageTags.map(function (item) {
                                return <span key={"tag" + item + Math.random()}>{item + " "}</span>;
                            })
                        }</div>
                        <ShareButton id="test"/>
                    </div>
                </div>
            </div>
        </section>
    }
}

export default Admin;