import React, {Component} from "react";
import "./Footer.css";
import NavLink from "react-router-dom/es/NavLink";

class Footer extends Component {
    render() {
        return <footer>
            <div className="container">
                    <NavLink exact to={'/'}
                             className="mainMenuLinks"
                             activeClassName="mainMenuLinks_active">Блог</NavLink>
                    <NavLink to={'/about'}
                             className="mainMenuLinks"
                             activeClassName="mainMenuLinks_active">О нас</NavLink>

                    <NavLink to={'/contacts'}
                             className="mainMenuLinks"
                             activeClassName="mainMenuLinks_active">Контакты</NavLink>
            </div>
        </footer>;
    }
}

export default Footer;