import React, {Component} from "react";
import "./Post.css";
import ShareButton from "./ShareButton/ShareButton.js";
import Markdown from "react-markdown";

class Post extends Component {

    render() {
        return <article>
            <h2>{this.props.item.name}</h2>
            <hr/>
            <p className="date">{this.props.item.date}</p>
            <div className="text">
                <Markdown source={this.props.item.text} escapeHtml={false}/>
            </div>
            <div className="flexPostFooter">
                <div className="tags">{
                    this.props.item.tags.map(function (item) {
                        return <span key={"tag" + item.id}>{item.tag + " "}</span>
                    })
                }</div>
                <ShareButton id={this.props.item.id}/>
            </div>
        </article>;
    }
}

export default Post;