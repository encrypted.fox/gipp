import React, {Component, Fragment} from "react";
import Post from "./Post/Post.js";
import icon from "../../../../media/logo.svg";

class PostsList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            items: [
                {
                    id: 1,
                    icon: icon,
                    name: "Красивая статья для красивых статейных статей",
                    date: "Time_Template",
                    text: "## Reasons React is great\n\n" +
                        "[google](https://google.com)\n\n" +
                        "1. Server-side rendering\n\n" +
                        "2. This totally works:\n\n" +
                        `<img class="textImage small" src= ${icon} alt=''/>` +
                        `\n\n` +
                        `* This\n\n` +
                        `* This`,
                    images: [{src: icon, id: 1}, {src: icon, id: 2}, {src: icon, id: 3}],
                    tags: [{id: 1, tag: "#tag"}, {id: 2, tag: "#tag"}, {id: 3, tag: "#tag"}]
                },
                {
                    id: 2,
                    icon: icon,
                    name: "Красивая статья для красивых статейных статей",
                    date: "Time_Template",
                    text: "# Reasons React is great\n\n" +
                        "## Reasons React is great\n\n" +
                        "### Reasons React is great\n\n" +
                        "#### Reasons React is great\n\n" +
                        "##### Reasons React is great\n\n" +
                        "Reasons React is great\n\n" +
                        "[google](https://google.com)\n\n" +
                        "1. Server-side rendering\n\n" +
                        "2. This totally works:\n\n" +
                        "- This\n\n",
                    images: [{src: icon, id: 1}, {src: icon, id: 2}, {src: icon, id: 3}],
                    tags: [{id: 1, tag: "#tag"}, {id: 2, tag: "#tag"}, {id: 3, tag: "#tag"}]
                },
                {
                    id: 3,
                    icon: icon,
                    name: "Красивая статья для красивых статейных статей",
                    date: "Time_Template",
                    text: "## Reasons React is great\n\n" +
                        "[google](https://google.com)\n\n" +
                        "1. Server-side rendering\n\n" +
                        "2. This totally works:\n\n" +
                        "- This\n\n",
                    images: [{src: icon, id: 1}, {src: icon, id: 2}, {src: icon, id: 3}],
                    tags: [{id: 1, tag: "#tag"}, {id: 2, tag: "#tag"}, {id: 3, tag: "#tag"}]
                }
            ]
        }
    }

    render() {
        return <Fragment>
            {
                this.state.items.map(function (item) {
                    return <Post key={item.id + Math.random()} name={item.id} item={item}/>
                })
            }
        </Fragment>


    }
}

export default PostsList;