import React, {Component} from "react";
import PostsList from "./PostsList/PostsList.js";
import Filters from "./Filters/Filters.js";

class Blog extends Component {
    render() {
        return <section className="container">
            <Filters/>
            <PostsList/>
        </section>;
    }
}

export default Blog;