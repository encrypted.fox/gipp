import './Menu.css';
import logo from "../../../media/logo.svg";
import menuIcon from "../../../media/ellipsis-h-solid.svg";
import React, {Component} from "react";
import NavLink from "react-router-dom/es/NavLink";


class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {menu: "linksGroup"};
        this.handleMenuClick = this.handleMenuClick.bind(this);
    }

    handleMenuClick() {
        let className = (this.state.menu === "linksGroup") ? "linksGroup on" : "linksGroup";
        this.setState({menu: className});
    }

    render() {
        return (
            <header>
                <nav className="container">
                    <NavLink exact to={'/'}>
                        <img className="navLogo" src={logo} alt="logo"/>
                    </NavLink>
                    <div className={this.state.menu} id="linksGroup">
                        <NavLink exact to={'/'}
                                 className="mainMenuLinks"
                                 onClick={this.handleMenuClick}
                                 activeClassName="mainMenuLinks_active">Блог</NavLink>
                        <NavLink to={'/about'}
                                 onClick={this.handleMenuClick}
                                 className="mainMenuLinks"
                                 activeClassName="mainMenuLinks_active">О нас</NavLink>
                        <NavLink to={'/contacts'}
                                 onClick={this.handleMenuClick}
                                 className="mainMenuLinks"
                                 activeClassName="mainMenuLinks_active">Контакты</NavLink>
                    </div>
                </nav>
                <img className="menuIcon" src={menuIcon} alt="menuIcon" onClick={this.handleMenuClick}/>
            </header>
        );
    }
}

export default Menu;