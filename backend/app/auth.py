from flask import Blueprint, make_response, abort, jsonify, request
import base64
import pymysql
import dao as dao

auth = Blueprint('auth', __name__)


def check(email, password):
    query = "email = '" + str(email) + "' AND password = TO_BASE64('" + str(password) + "')"
    user = dao.select("users", "user_id, email, role, name, group_id, password", query)
    if len(user) == 1:
        return True
    else:
        return False


@auth.route('/check', methods=['POST'])
@auth.route('/check/', methods=['POST'])
def check_auth():
    if "email" not in request.json or "password" not in request.json or not request.json:
        abort(403)
        email = pymysql.escape_string(request.json["email"])
        password = pymysql.escape_string(request.json["password"])
        check(email, password)
        if check(email, password):
            response = make_response("authorized", 200)
            response.headers = {"Access-Control-Allow-Origin": "*"}
            return response
        else:
            abort(403)
    return make_response("Error", 403)
