from flask import Blueprint, make_response, abort, jsonify, request
import pymysql
import dao as dao

users_page = Blueprint('users_page', __name__)

# table users manipulations

create_users = "user_id INT(10) PRIMARY KEY AUTO_INCREMENT, " \
               "group_id INT(10), " \
               "role INT(1), " \
               "name VARCHAR(100), " \
               "email VARCHAR(100) UNIQUE, " \
               "password VARCHAR(100)"
dao.create("users", create_users)


@users_page.route("/users", methods=['OPTIONS'])
@users_page.route("/users/", methods=['OPTIONS'])
def ret_opt_for_users():
    response = make_response(200)
    response.headers = {"Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Methods": "POST, GET, PUT, PATCH, DELETE"}
    return response


@users_page.route('/users', methods=['GET'])
@users_page.route('/users/', methods=['GET'])
def get_users():
    users = dao.select("users", "user_id, email, role, name, group_id", 1)
    response = make_response(jsonify({'users': users}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@users_page.route('/users/<int:user_id>', methods=['GET'])
def get_user_by_id(user_id):
    query = "user_id=" + str(user_id)
    user = dao.select("users", "user_id, email, role, name, group_id", query)

    if len(user) == 0:
        return abort(404)

    response = make_response(jsonify({'user': user[0]}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@users_page.route('/users/<string:email>', methods=['GET'])
def get_user_by_email(email):
    query = 'email = "' + email + '"'
    user = dao.select("users", "user_id, email, role, name, group_id", query)

    if len(user) == 0:
        abort(404)

    response = make_response(jsonify({'user': user[0]}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@users_page.route('/users', methods=['POST'])
@users_page.route('/users/', methods=['POST'])
def insert_user():
    if not request.json or "role" not in request.json or "group_id" not in request.json or "name" not in request.json or "password" not in request.json or "email" not in request.json:
        abort(400)

    role = int(pymysql.escape_string(str(request.json['role'])))
    group_id = int(pymysql.escape_string(str(request.json['group_id'])))
    name = pymysql.escape_string(request.json['name'])
    password = pymysql.escape_string(request.json['password'])
    email = pymysql.escape_string(request.json['email'])

    query = str(role) + ", " + str(group_id) + ', "' + str(name) + '", TO_BASE64("' + str(password) + '"), "' + str(
        email) + '"'
    dao.insert("users", "role, group_id, name, password, email", query)

    users = dao.select("users", "user_id, email, role, name, group_id", 1)
    response = make_response(jsonify({'users': users}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@users_page.route('/users/<string:email>', methods=['PATCH'])
def update_user(email):
    if not request.json:
        abort(400)

    select_query = 'email = "' + str(email) + '"'
    user = dao.select("users", "*", select_query)
    query = ""

    if len(user) == 0:
        abort(404)

    if "role" in request.json:
        role = int(pymysql.escape_string(str(request.json['role'])))
    else:
        role = None

    if "group_id" in request.json:
        group_id = int(pymysql.escape_string(str(request.json['group_id'])))
    else:
        group_id = None

    if "name" in request.json:
        name = pymysql.escape_string(request.json['name'])
    else:
        name = None

    if "email" in request.json:
        email_query = pymysql.escape_string(request.json['email'])
    else:
        email_query = None

    if "password" in request.json:
        password = pymysql.escape_string(request.json['password'])
    else:
        password = None

    if role is not None:
        query += "role = " + str(role) + ", "
    else:
        query += "role = " + str(user[0]["role"]) + ", "

    if group_id is not None:
        query += "group_id = " + str(group_id) + ", "
    else:
        query += "group_id = " + str(user[0]["group_id"]) + ", "

    if name is not None:
        query += 'name = "' + str(name) + '", '
    else:
        query += 'name = "' + str(user[0]["name"]) + '", '

    if email_query is not None:
        query += 'email = "' + str(email_query) + '", '
    else:
        query += 'email = "' + str(user[0]["email"]) + '", '

    if password is not None:
        query += 'password = TO_BASE64("' + str(password) + '")'
    else:
        query += 'password = "' + str(user[0]['password']) + '"'

    if query != "":
        update_query = "email = '" + user[0]["email"] + "'"
        dao.update("users", query, update_query)

    users = dao.select("users", "user_id, email, role, name, group_id", 1)
    response = make_response(jsonify({'users': users}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@users_page.route('/users/<string:email>', methods=['DELETE'])
def delete_user(email):
    if "email" not in request.json or "password" not in request.json or not request.json:
        abort(403)
    else:
        email_query = pymysql.escape_string(request.json["email"])
        password = pymysql.escape_string(request.json["password"])

        query = "email = '" + str(email_query) + "' AND password = TO_BASE64('" + str(password) + "')"
        user = dao.select("users", "*", query)

        if len(user) == 0:
            abort(403)
        else:
            if int(user[0]["role"]) >= 5:
                where = "email = '" + str(email) + "'"
                dao.delete('users', where)

                users = dao.select("users", "user_id, email, role, name, group_id", 1)
                response = make_response(jsonify({'users': users}), 200)
                response.headers = {"Access-Control-Allow-Origin": "*"}
                return response
    return make_response("Error", 403)


@users_page.route("/drop/users", methods=['POST'])
@users_page.route("/drop/users/", methods=['POST'])
def drop_table_users():
    if "email" not in request.json or "password" not in request.json or not request.json:
        abort(403)
    else:
        email_query = pymysql.escape_string(request.json["email"])
        password = pymysql.escape_string(request.json["password"])

        query = "email = '" + str(email_query) + "' AND password = TO_BASE64('" + str(password) + "')"
        user = dao.select("users", "*", query)

        if len(user) == 0:
            abort(403)
        else:
            if int(user[0]["role"]) >= 5:
                dao.drop("users")
                response = make_response("Dropped table users", 200)
                response.headers = {"Access-Control-Allow-Origin": "*"}
                return response
    return make_response("Error", 403)
