from flask import Blueprint, make_response, abort, jsonify, request, render_template
import pymysql

static_routes = Blueprint('static_routes', __name__)


@static_routes.route("/constructor")
@static_routes.route("/constructor.html")
def ret_const_for_users():
    return render_template("constructor.html")


@static_routes.route("/personalAccaunt")
@static_routes.route("/personalAccaunt.html")
def ret_per_for_users():
    return render_template("personalAccaunt.html")


@static_routes.route("/constructor-1")
@static_routes.route("/constructor-1.html")
def ret_const_1_for_users():
    return render_template("constructor-1.html")


@static_routes.route("/")
@static_routes.route("/index")
@static_routes.route("/index.html")
def ret_ind_for_users():
    return render_template("index.html")


@static_routes.route("/index-1.html")
@static_routes.route("/index-1")
def ret_ind_1_for_users():
    return render_template("index-1.html")


@static_routes.route("/menu-1.html")
@static_routes.route("/menu-1")
def ret_men_for_users():
    return render_template("menu-1.html")


@static_routes.route("/menu-2.html")
@static_routes.route("/menu-2")
def ret_men_2_for_users():
    return render_template("menu-2.html")