# -*- coding: utf-8 -*-
import pymysql
from pymysql.cursors import DictCursor
from flask import abort


def connect():
    try:
        _connection = pymysql.connect(
            host='localhost',
            user='mysql',
            password='mysql',
            charset='UTF8',
            cursorclass=DictCursor)
        _cursor = _connection.cursor()
        _cursor._defer_warnings = True
        _cursor.execute("CREATE DATABASE IF NOT EXISTS base;")
        _cursor.execute("USE base;")
    except pymysql.OperationalError:
        return "No connection to database..."
    return _connection


# def connect():
#     try:
#         _connection = pymysql.connect(
#             host='172.17.0.2',
#             user='root',
#             password='mysql',
#             charset='UTF8',
#             cursorclass=DictCursor)
#         _cursor = _connection.cursor()
#         _cursor.execute("CREATE DATABASE IF NOT EXISTS base;")
#         _cursor.execute("USE base;")
#     except pymysql.OperationalError:
#         return "No connection to database..."
#     return _connection


connection = connect()
try:
    cursor = connection.cursor()
    cursor._defer_warnings = True
except AttributeError:
    print("Database not Connected")
    abort(500)


def create(tableName, arguments):
    create_query = "CREATE TABLE IF NOT EXISTS " + tableName + "( " + arguments + " ) CHARACTER SET utf8;"
    try:
        execute(create_query)
        return True
    except:
        pass
    return False


def create_db(baseName):
    create_query = "CREATE DATABASE IF NOT EXISTS " + baseName + ";"
    try:
        execute(create_query)
        return True
    except:
        return False


def select(tableName, selectQuery, where):
    if where is not None:
        select_query = "SELECT " + selectQuery + " FROM " + tableName + " WHERE " + str(where) + ";"
    else:
        select_query = "SELECT " + selectQuery + " FROM " + tableName + ";"
    cursor.execute(select_query)
    selected = cursor.fetchall()
    return selected


def insert(tableName, columnsNames, values):
    insert_query = "INSERT INTO " + tableName + "(" + columnsNames + ") VALUES(" + values + ");"
    execute(insert_query)
    return


def update(tableName, values, where):
    update_query = "UPDATE " + tableName + " SET " + values + " WHERE " + str(where) + ";"
    execute(update_query)
    return


def delete(tableName, where):
    delete_query = "DELETE FROM " + tableName + " WHERE " + str(where) + ";"
    execute(delete_query)
    return


def drop(tableName):
    drop_query = "DROP TABLE " + tableName + ";"
    execute(drop_query)
    return


def execute(query):
    cursor.execute(query)
    connection.commit()
    return


def close():
    return connection.close()
