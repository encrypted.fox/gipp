from flask import Blueprint, make_response, abort, jsonify, request
import pymysql
import dao as dao

sites_page = Blueprint('sites_page', __name__)

# table sites manipulations

create_sites = "site_id INT(10) PRIMARY KEY AUTO_INCREMENT, " \
               "group_id INT(10), " \
               "content TEXT, " \
               "name VARCHAR(100)"
dao.create("sites", create_sites)


@sites_page.route("/sites", methods=['OPTIONS'])
@sites_page.route("/sites/", methods=['OPTIONS'])
def ret_opt_for_sites():
    response = make_response(200)
    response.headers = {"Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Methods": "POST, GET, PUT, PATCH, DELETE"}
    return response


@sites_page.route('/sites', methods=['GET'])
@sites_page.route('/sites/', methods=['GET'])
def get_sites():
    sites = dao.select("sites", "*", 1)
    response = make_response(jsonify({'sites': sites}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@sites_page.route('/sites/<int:site_id>', methods=['GET'])
def get_site_by_id(site_id):
    query = "site_id=" + str(site_id)
    site = dao.select("sites", "*", query)

    if len(site) == 0:
        abort(404)

    response = make_response(jsonify({'site': site[0]}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@sites_page.route('/sites', methods=['POST'])
@sites_page.route('/sites/', methods=['POST'])
def insert_site():
    if not request.json or "group_id" not in request.json or "name" not in request.json or "content" not in request.json:
        abort(400)

    group_id = int(pymysql.escape_string(str(request.json['group_id'])))
    name = pymysql.escape_string(request.json['name'])
    content = request.json['content']

    query = '"' + str(group_id) + '", "' + str(name) + '", "' + str(content) + '"'
    dao.insert("sites", "group_id, name, content", query)

    sites = dao.select("sites", "*", 1)

    response = make_response(jsonify({'sites': sites}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@sites_page.route('/sites/<int:site_id>', methods=['PATCH'])
def update_site(site_id):
    if not request.json:
        abort(400)

    select_query = 'site_id = "' + str(site_id) + '"'
    site = dao.select("sites", "*", select_query)
    query = ""

    if len(user) == 0:
        abort(404)

    if "group_id" in request.json:
        group_id = int(pymysql.escape_string(str(request.json['group_id'])))
    else:
        group_id = None

    if "name" in request.json:
        name = pymysql.escape_string(request.json['name'])
    else:
        name = None

    if "content" in request.json:
        content = pymysql.escape_string(request.json['content'])
    else:
        content = None

    if group_id is not None:
        query += "group_id = " + str(group_id) + ", "
    else:
        query += "group_id = " + str(site[0]["group_id"]) + ", "

    if name is not None:
        query += 'name = "' + str(name) + '", '
    else:
        query += 'name = "' + str(site[0]["name"]) + '", '

    if content is not None:
        query += 'content = "' + str(content) + '", '
    else:
        query += 'content = "' + str(site[0]["content"]) + '", '

    if query != "":
        update_query = "site_id = '" + site[0]["site_id"] + "'"
        dao.update("sites", query, update_query)

    sites = dao.select("sites", "*", 1)
    response = make_response(jsonify({'sites': sites}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@sites_page.route('/sites/<int:site_id>', methods=['DELETE'])
def delete_site(site_id):
    if "email" not in request.json or "password" not in request.json or not request.json:
        abort(403)
    else:
        email = pymysql.escape_string(request.json["email"])
        password = pymysql.escape_string(request.json["password"])

        query = "email = '" + str(email) + "' AND password = TO_BASE64('" + str(password) + "')"
        user = dao.select("users", "*", query)

        if len(user) == 0:
            abort(403)
        else:
            if int(user[0]["role"]) >= 5:
                where = "site_id = '" + str(site_id) + "'"
                dao.delete('sites', where)

                sites = dao.select("sites", "*", 1)
                response = make_response(jsonify({'sites': sites}), 200)
                response.headers = {"Access-Control-Allow-Origin": "*"}
                return response
    return make_response("Error", 403)


@sites_page.route("/drop/sites", methods=['POST'])
@sites_page.route("/drop/sites/", methods=['POST'])
def drop_table_sites():
    if "email" not in request.json or "password" not in request.json or not request.json:
        abort(403)
    else:
        email = pymysql.escape_string(request.json["email"])
        password = pymysql.escape_string(request.json["password"])

        query = "email = '" + str(email) + "' AND password = TO_BASE64('" + str(password) + "')"
        user = dao.select("users", "*", query)

        if len(user) == 0:
            abort(403)
        else:
            if int(user[0]["role"]) >= 5:
                dao.drop("sites")
                response = make_response("Dropped table sites", 200)
                response.headers = {"Access-Control-Allow-Origin": "*"}
                return response
    return make_response("Error", 403)
