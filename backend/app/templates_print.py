from flask import Blueprint, make_response, abort, jsonify, request
import pymysql
import dao as dao

templates_page = Blueprint('templates_page', __name__)

# table templates manipulations

create_templates = "template_id INT(10) PRIMARY KEY AUTO_INCREMENT, " \
                   "content TEXT, " \
                   "name VARCHAR(100)"
dao.create("templates", create_templates)


@templates_page.route("/templates", methods=['OPTIONS'])
def ret_opt_for_templates():
    response = make_response(200)
    response.headers = {"Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Methods": "POST, GET, PUT, PATCH, DELETE"}
    return response


@templates_page.route('/templates', methods=['GET'])
@templates_page.route('/templates/', methods=['GET'])
def get_templates():
    templates = dao.select("templates", "*", 1)
    response = make_response(jsonify({'templates': templates}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@templates_page.route('/templates/<int:template_id>', methods=['GET'])
def get_template_by_id(template_id):
    query = "template_id=" + str(template_id)
    template = dao.select("templates", "*", query)

    if len(template) == 0:
        abort(404)

    response = make_response(jsonify({'template': template[0]}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@templates_page.route('/templates', methods=['POST'])
@templates_page.route('/templates/', methods=['POST'])
def insert_template():
    if not request.json or "name" not in request.json or "content" not in request.json:
        abort(400)

    name = pymysql.escape_string(request.json['name'])
    content = request.json['content']

    query = '"' + str(name) + '", "' + str(content) + '"'
    dao.insert("templates", "name, content", query)

    templates = dao.select("templates", "*", 1)

    response = make_response(jsonify({'templates': templates}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@templates_page.route('/templates/<int:template_id>', methods=['PATCH'])
def update_template(template_id):
    if not request.json:
        abort(400)

    select_query = 'template_id = "' + str(template_id) + '"'
    template = dao.select("templates", "*", select_query)
    query = ""

    if "name" in request.json:
        name = pymysql.escape_string(request.json['name'])
    else:
        name = None

    if "content" in request.json:
        content = pymysql.escape_string(request.json['content'])
    else:
        content = None

    if name is not None:
        query += 'name = "' + str(name) + '", '
    else:
        query += 'name = "' + str(template[0]["name"]) + '", '

    if content is not None:
        query += 'content = "' + str(content) + '", '
    else:
        query += 'content = "' + str(template[0]["content"]) + '", '

    if query != "":
        update_query = "template_id = '" + template[0]["template_id"] + "'"
        dao.update("templates", query, update_query)

    templates = dao.select("templates", "*", 1)
    response = make_response(jsonify({'templates': templates}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@templates_page.route('/templates/<int:template_id>', methods=['DELETE'])
def delete_template(template_id):
    if "email" not in request.json or "password" not in request.json or not request.json:
        abort(403)
    else:
        email = pymysql.escape_string(request.json["email"])
        password = pymysql.escape_string(request.json["password"])

        query = "email = '" + str(email) + "' AND password = TO_BASE64('" + str(password) + "')"
        user = dao.select("users", "*", query)

        if len(user) == 0:
            abort(403)
        else:
            if int(user[0]["role"]) >= 5:
                where = "template_id = " + str(template_id)
                dao.delete('templates', where)

                templates = dao.select("templates", "*", None)
                response = make_response(jsonify({'templates': templates}), 200)
                response.headers = {"Access-Control-Allow-Origin": "*"}
                return response
    return make_response("Error", 403)


@templates_page.route("/drop/templates", methods=['GET'])
@templates_page.route("/drop/templates/", methods=['GET'])
def drop_table_templates():
    if "email" not in request.json or "password" not in request.json or not request.json:
        abort(403)
    else:
        email = pymysql.escape_string(request.json["email"])
        password = pymysql.escape_string(request.json["password"])

        query = "email = '" + str(email) + "' AND password = TO_BASE64('" + str(password) + "')"
        user = dao.select("users", "*", query)

        if len(user) == 0:
            abort(403)
        else:
            if int(user[0]["role"]) >= 5:
                dao.drop("templates")
                response = make_response("Dropped table templates", 200)
                response.headers = {"Access-Control-Allow-Origin": "*"}
                return response
    return make_response("Error", 403)
