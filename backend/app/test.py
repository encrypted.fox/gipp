import unittest
import dao
import auth
import users


class Test(unittest.TestCase):
    def test_connect(self):
        self.assertNotEqual("No connection to database...", dao.connect())

    def test_create_db(self):
        self.assertEqual(dao.create_db("base"), True)

    def test_create(self):
        create_users = "user_id INT(10) PRIMARY KEY AUTO_INCREMENT, " \
                       "group_id INT(10), " \
                       "role INT(1), " \
                       "name VARCHAR(100), " \
                       "email VARCHAR(100) UNIQUE, " \
                       "password VARCHAR(100)"
        self.assertEqual(dao.create("users", create_users), True)

        create_users = "user_id INT(10) PRIMARY KEY AUTO_INCREMENT, " \
                       "group_id INT(10), " \
                       "role INT(1) " \
                       "name VARCHAR(100), " \
                       "email VARCHAR(100) UNIQUE, " \
                       "password VARCHAR(100)"
        self.assertEqual(dao.create("users", create_users), False)

    def test_insert_select_delete(self):
        dao.delete("users", "email='test'")

        firstArgument = str(dao.select("users", "group_id, role, name, email", "email='test'"))
        secondArgument = "[{'group_id': 0, 'role': 0, 'name': 'test', 'email': 'test'}]"
        self.assertNotEqual(firstArgument, secondArgument)

        insert_query = "'0', '0', 'test', 'test', TO_BASE64('233edf')"
        dao.insert("users", "group_id, role, name, email, password", insert_query)

        firstArgument = str(dao.select("users", "group_id, role, name, email", "email='test'"))
        secondArgument = "[{'group_id': 0, 'role': 0, 'name': 'test', 'email': 'test'}]"
        self.assertEqual(firstArgument, secondArgument)

        dao.delete("users", "email='test'")

        firstArgument = str(dao.select("users", "group_id, role, name, email", "email='test'"))
        secondArgument = "[{'group_id': 0, 'role': 0, 'name': 'test', 'email': 'test'}]"
        self.assertNotEqual(firstArgument, secondArgument)

    def test_auth(self):
        dao.delete("users", "email='test'")
        dao.insert("users", "group_id, role, name, email, password", "'0', '0', 'test', 'test', TO_BASE64('233edf')")

        self.assertEqual(True, auth.check("test", "233edf"))
        self.assertEqual(False, auth.check("test", "233edfdf"))
        self.assertEqual(False, auth.check("else", "233edf"))

        dao.delete("users", "email='test'")
        self.assertEqual(False, auth.check("test", "233edf"))


if __name__ == '__main__':
    unittest.main()