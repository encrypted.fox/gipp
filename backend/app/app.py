import dao as dao
from flask import Flask, render_template, abort

app = Flask(__name__)

dao.connect()

from users import users_page

app.register_blueprint(users_page)
from sites import sites_page

app.register_blueprint(sites_page)
from templates_print import templates_page

app.register_blueprint(templates_page)
from posts_page import posts_page

app.register_blueprint(posts_page)
from auth import auth

app.register_blueprint(auth)
from static_routes import static_routes

app.register_blueprint(static_routes)


@app.errorhandler(404)
def not_found():
    return render_template("404.htm")


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=3000, debug=True)
