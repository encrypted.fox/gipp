from flask import Blueprint, make_response, abort, jsonify, request
import pymysql
import dao as dao

posts_page = Blueprint('posts_page', __name__)

# table posts manipulations

create_posts = "post_id INT(10) PRIMARY KEY AUTO_INCREMENT, " \
               "body TEXT, " \
               "site_id INT(10), " \
               "user_id INT(10), " \
               "group_id INT(10), " \
               "status VARCHAR(100), " \
               "timestamp DATE"
dao.create("posts", create_posts)


@posts_page.route("/posts", methods=['OPTIONS'])
@posts_page.route("/posts/", methods=['OPTIONS'])
def ret_opt_for_posts():
    response = make_response(200)
    response.headers = {"Access-Control-Allow-Origin": "*",
                        "Access-Control-Allow-Methods": "POST, GET, PUT, PATCH, DELETE"}
    return response


@posts_page.route('/posts', methods=['GET'])
@posts_page.route('/posts/', methods=['GET'])
def get_posts():
    posts = dao.select("posts", "*", 1)
    response = make_response(jsonify({'posts': posts}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@posts_page.route('/posts/<int:post_id>', methods=['GET'])
def get_post_by_id(post_id):
    query = "post_id=" + str(post_id)
    post = dao.select("posts", "*", query)

    if len(post) == 0:
        abort(404)

    response = make_response(jsonify({'post': post[0]}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@posts_page.route('/posts', methods=['POST'])
@posts_page.route('/posts/', methods=['POST'])
def insert_post():
    if not request.json or "user_id" not in request.json or "group_id" not in request.json or "site_id" not in request.json or "status" not in request.json or "timestamp" not in request.json or "body" not in request.json:
        abort(400)

    user_id = int(pymysql.escape_string(str(request.json['user_id'])))
    group_id = int(pymysql.escape_string(str(request.json['group_id'])))
    site_id = pymysql.escape_string(request.json['site_id'])
    status = pymysql.escape_string(request.json['status'])
    timestamp = pymysql.escape_string(request.json['timestamp'])
    body = request.json['body']

    query = '"' + str(status) + '", "' + str(body) + '", "' + str(user_id) + '", "' + str(site_id) + '", "' + str(
        group_id) + '", "' + str(
        timestamp) + '"'
    dao.insert("posts", "status, body, user_id, site_id, group_id, timestamp", query)

    posts = dao.select("posts", "*", 1)

    response = make_response(jsonify({'posts': posts}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@posts_page.route('/posts/<int:post_id>', methods=['PATCH'])
def update_post(post_id):
    if not request.json:
        abort(400)

    select_query = 'post_id = "' + str(post_id) + '"'
    post = dao.select("posts", "*", select_query)
    query = ""

    if "user_id" in request.json:
        user_id = int(pymysql.escape_string(str(request.json['user_id'])))
    else:
        user_id = None

    if "site_id" in request.json:
        site_id = int(pymysql.escape_string(str(request.json['site_id'])))
    else:
        site_id = None

    if "group_id" in request.json:
        group_id = int(pymysql.escape_string(str(request.json['group_id'])))
    else:
        group_id = None

    if "status" in request.json:
        status = pymysql.escape_string(request.json['status'])
    else:
        status = None

    if "body" in request.json:
        body = pymysql.escape_string(request.json['body'])
    else:
        body = None

    if "timestamp" in request.json:
        timestamp = pymysql.escape_string(request.json['timestamp'])
    else:
        timestamp = None

    if user_id is not None:
        query += 'user_id = "' + str(user_id) + '", '
    else:
        query += 'user_id = "' + str(post[0]["user_id"]) + '", '

    if site_id is not None:
        query += 'site_id = "' + str(site_id) + '", '
    else:
        query += 'site_id = "' + str(post[0]["site_id"]) + '", '

    if group_id is not None:
        query += 'group_id = "' + str(group_id) + '", '
    else:
        query += 'group_id = "' + str(post[0]["group_id"]) + '", '

    if status is not None:
        query += 'status = "' + str(status) + '", '
    else:
        query += 'status = "' + str(post[0]["status"]) + '", '

    if body is not None:
        query += 'body = "' + str(body) + '", '
    else:
        query += 'body = "' + str(post[0]["body"]) + '", '

    if timestamp is not None:
        query += 'timestamp = "' + str(timestamp) + '", '
    else:
        query += 'timestamp = "' + str(post[0]["timestamp"]) + '", '

    if query != "":
        update_query = "post_id = '" + post[0]["post_id"] + "'"
        dao.update("posts", query, update_query)

    posts = dao.select("posts", "*", 1)
    response = make_response(jsonify({'posts': posts}), 200)
    response.headers = {"Access-Control-Allow-Origin": "*"}
    return response


@posts_page.route('/posts/<int:post_id>', methods=['DELETE'])
def delete_post(post_id):
    if "email" not in request.json or "password" not in request.json or not request.json:
        abort(403)
    else:
        email = pymysql.escape_string(request.json["email"])
        password = pymysql.escape_string(request.json["password"])

        query = "email = '" + str(email) + "' AND password = TO_BASE64('" + str(password) + "')"
        user = dao.select("users", "*", query)

        if len(user) == 0:
            abort(403)
        else:
            if int(user[0]["role"]) >= 5:
                where = "post_id = " + str(post_id)
                dao.delete('posts', where)

                posts = dao.select("posts", "*", None)
                response = make_response(jsonify({'posts': posts}), 200)
                response.headers = {"Access-Control-Allow-Origin": "*"}
                return response
    return make_response("Error", 403)


@posts_page.route("/drop/posts", methods=['GET'])
@posts_page.route("/drop/posts/", methods=['GET'])
def drop_table_posts():
    if "email" not in request.json or "password" not in request.json or not request.json:
        abort(403)
    else:
        email = pymysql.escape_string(request.json["email"])
        password = pymysql.escape_string(request.json["password"])

        query = "email = '" + str(email) + "' AND password = TO_BASE64('" + str(password) + "')"
        user = dao.select("users", "*", query)

        if len(user) == 0:
            abort(403)
        else:
            if int(user[0]["role"]) >= 5:
                dao.drop("posts")
                response = make_response("Dropped table posts", 200)
                response.headers = {"Access-Control-Allow-Origin": "*"}
                return response
    return make_response("Error", 403)
